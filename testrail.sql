-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Client :  localhost:3306
-- Généré le :  Mar 07 Août 2018 à 08:07
-- Version du serveur :  5.7.23-0ubuntu0.18.04.1
-- Version de PHP :  5.6.37-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `testrail`
--

-- --------------------------------------------------------

--
-- Structure de la table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `start_date` int(11) DEFAULT NULL,
  `end_date` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `width` int(11) DEFAULT NULL,
  `view` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `attachments`
--

CREATE TABLE `attachments` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `filename` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `case_id` int(11) DEFAULT NULL,
  `test_change_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `attachments`
--

INSERT INTO `attachments` (`id`, `name`, `filename`, `size`, `created_on`, `project_id`, `case_id`, `test_change_id`, `user_id`) VALUES
(1, 'Capture du 2018-07-19 00-29-29.png', '1.capture_du_2018_07_19_00_29_29.png', 12120, 1531960203, 3, NULL, NULL, NULL),
(2, 'Capture du 2018-07-19 00-33-22.png', '2.capture_du_2018_07_19_00_33_22.png', 9936, 1531960427, 3, NULL, NULL, NULL),
(3, 'Capture du 2018-07-19 00-35-33.png', '3.capture_du_2018_07_19_00_35_33.png', 9120, 1531960558, 3, NULL, NULL, NULL),
(4, 'Capture du 2018-07-19 00-36-59.png', '4.capture_du_2018_07_19_00_36_59.png', 9107, 1531960646, 3, NULL, NULL, NULL),
(5, 'Capture du 2018-07-19 00-36-59.png', '5.capture_du_2018_07_19_00_36_59.png', 9107, 1531960749, 3, NULL, NULL, NULL),
(6, 'Capture du 2018-07-19 00-36-59.png', '6.capture_du_2018_07_19_00_36_59.png', 9107, 1531960803, 3, NULL, NULL, NULL),
(7, 'Capture du 2018-07-19 00-25-04.png', '7.capture_du_2018_07_19_00_25_04.png', 75981, 1531961047, 3, NULL, 1, NULL),
(8, 'Capture du 2018-07-19 00-24-06.png', '8.capture_du_2018_07_19_00_24_06.png', 71453, 1531961073, 3, NULL, 2, NULL),
(9, 'Capture du 2018-07-19 00-24-43.png', '9.capture_du_2018_07_19_00_24_43.png', 75659, 1531961086, 3, NULL, 3, NULL),
(10, 'Capture du 2018-07-19 00-25-04.png', '10.capture_du_2018_07_19_00_25_04.png', 75981, 1531961544, 3, NULL, NULL, NULL),
(11, 'Capture du 2018-07-19 00-24-06.png', '11.capture_du_2018_07_19_00_24_06.png', 71453, 1531961626, 3, NULL, NULL, NULL),
(12, 'Capture du 2018-07-19 00-24-43.png', '12.capture_du_2018_07_19_00_24_43.png', 75659, 1531961688, 3, NULL, NULL, NULL),
(13, 'Capture du 2018-08-06 08-51-31.png', '13.capture_du_2018_08_06_08_51_31.png', 20107, 1533545515, 4, NULL, NULL, NULL),
(14, 'Capture du 2018-08-06 08-53-59.png', '14.capture_du_2018_08_06_08_53_59.png', 20581, 1533545651, 4, NULL, NULL, NULL),
(15, 'Capture du 2018-08-06 08-55-24.png', '15.capture_du_2018_08_06_08_55_24.png', 25470, 1533545944, 4, NULL, NULL, NULL),
(16, 'Capture du 2018-08-06 08-55-24.png', '16.capture_du_2018_08_06_08_55_24.png', 25470, 1533546127, 4, NULL, NULL, NULL),
(17, 'Capture du 2018-08-06 09-25-27.png', '17.capture_du_2018_08_06_09_25_27.png', 20620, 1533547549, 4, NULL, NULL, NULL),
(18, 'Capture du 2018-08-06 09-26-40.png', '18.capture_du_2018_08_06_09_26_40.png', 24913, 1533547610, 4, NULL, NULL, NULL),
(19, 'Capture du 2018-08-06 08-51-31.png', '19.capture_du_2018_08_06_08_51_31.png', 20107, 1533547774, 4, NULL, NULL, NULL),
(20, 'Capture du 2018-08-06 09-30-05.png', '20.capture_du_2018_08_06_09_30_05.png', 20496, 1533547827, 4, NULL, NULL, NULL),
(21, 'Capture du 2018-08-06 09-31-29.png', '21.capture_du_2018_08_06_09_31_29.png', 25064, 1533547905, 4, NULL, NULL, NULL),
(22, 'Capture du 2018-08-06 08-51-31.png', '22.capture_du_2018_08_06_08_51_31.png', 20107, 1533548110, 4, NULL, NULL, NULL),
(23, 'Capture du 2018-08-06 09-35-54.png', '23.capture_du_2018_08_06_09_35_54.png', 10628, 1533548201, 4, NULL, NULL, NULL),
(24, 'Capture du 2018-08-06 08-51-31.png', '24.capture_du_2018_08_06_08_51_31.png', 20107, 1533548923, 4, NULL, NULL, NULL),
(25, 'Capture du 2018-08-06 09-49-13.png', '25.capture_du_2018_08_06_09_49_13.png', 20712, 1533549006, 4, NULL, NULL, NULL),
(26, 'Capture du 2018-08-06 09-50-29.png', '26.capture_du_2018_08_06_09_50_29.png', 8684, 1533549061, 4, NULL, NULL, NULL),
(27, 'Capture du 2018-08-06 08-51-31.png', '27.capture_du_2018_08_06_08_51_31.png', 20107, 1533549152, 4, NULL, NULL, NULL),
(28, 'Capture du 2018-08-06 09-54-31.png', '28.capture_du_2018_08_06_09_54_31.png', 21090, 1533549283, 4, NULL, NULL, NULL),
(29, 'Capture du 2018-08-06 09-50-29.png', '29.capture_du_2018_08_06_09_50_29.png', 8684, 1533549355, 4, NULL, NULL, NULL),
(30, 'Capture du 2018-08-06 10-17-17.png', '30.capture_du_2018_08_06_10_17_17.png', 220558, 1533550714, 4, NULL, 9, NULL),
(31, 'Capture du 2018-08-06 10-17-17.png', '31.capture_du_2018_08_06_10_17_17.png', 220558, 1533550744, 4, NULL, NULL, NULL),
(32, 'Capture du 2018-08-06 10-20-39.png', '32.capture_du_2018_08_06_10_20_39.png', 220908, 1533550853, 4, NULL, NULL, NULL),
(33, 'Capture du 2018-08-06 10-22-12.png', '33.capture_du_2018_08_06_10_22_12.png', 211123, 1533550964, 4, NULL, NULL, NULL),
(34, 'Capture du 2018-08-06 10-23-58.png', '34.capture_du_2018_08_06_10_23_58.png', 215813, 1533551048, 4, NULL, NULL, NULL),
(35, 'Capture du 2018-08-06 10-24-38.png', '35.capture_du_2018_08_06_10_24_38.png', 218446, 1533551103, 4, NULL, NULL, NULL),
(36, 'Capture du 2018-08-06 10-26-22.png', '36.capture_du_2018_08_06_10_26_22.png', 218291, 1533551190, 4, NULL, NULL, NULL),
(37, 'Capture du 2018-08-06 08-51-31.png', '37.capture_du_2018_08_06_08_51_31.png', 20107, 1533553560, 4, NULL, NULL, NULL),
(38, 'Capture du 2018-08-07 07-26-13.png', '38.capture_du_2018_08_07_07_26_13.png', 205107, 1533627018, 4, NULL, NULL, NULL),
(39, 'Capture du 2018-08-07 07-32-54.png', '39.capture_du_2018_08_07_07_32_54.png', 205465, 1533627213, 4, NULL, NULL, NULL),
(40, 'Capture du 2018-08-07 07-40-17.png', '40.capture_du_2018_08_07_07_40_17.png', 232082, 1533627630, 4, NULL, NULL, NULL),
(41, 'Capture du 2018-08-07 07-47-30.png', '41.capture_du_2018_08_07_07_47_30.png', 189552, 1533628072, 4, NULL, NULL, NULL),
(42, 'Capture du 2018-08-07 07-51-17.png', '42.capture_du_2018_08_07_07_51_17.png', 204465, 1533628393, 4, NULL, NULL, NULL),
(43, 'Capture du 2018-08-07 07-54-41.png', '43.capture_du_2018_08_07_07_54_41.png', 228548, 1533628492, 4, NULL, NULL, NULL),
(44, 'Capture du 2018-08-07 07-56-18.png', '44.capture_du_2018_08_07_07_56_18.png', 222852, 1533628610, 4, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `cases`
--

CREATE TABLE `cases` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `title` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `display_order` int(11) NOT NULL,
  `priority_id` int(11) NOT NULL,
  `estimate` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `custom_preconds` longtext COLLATE utf8_unicode_ci,
  `custom_steps` longtext COLLATE utf8_unicode_ci,
  `custom_expected` longtext COLLATE utf8_unicode_ci,
  `custom_steps_separated` longtext COLLATE utf8_unicode_ci,
  `custom_mission` longtext COLLATE utf8_unicode_ci,
  `custom_goals` longtext COLLATE utf8_unicode_ci,
  `custom_automation_type` int(11) DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `is_copy` tinyint(1) NOT NULL,
  `copyof_id` int(11) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `estimate_forecast` int(11) DEFAULT NULL,
  `refs` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `suite_id` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `template_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `cases`
--

INSERT INTO `cases` (`id`, `section_id`, `title`, `display_order`, `priority_id`, `estimate`, `milestone_id`, `custom_preconds`, `custom_steps`, `custom_expected`, `custom_steps_separated`, `custom_mission`, `custom_goals`, `custom_automation_type`, `type_id`, `is_copy`, `copyof_id`, `created_on`, `user_id`, `estimate_forecast`, `refs`, `suite_id`, `updated_on`, `updated_by`, `template_id`) VALUES
(1, 1, 'positif + positif', 1, 2, NULL, NULL, 'les champs sont vides\r\n\r\n![](index.php?/attachments/get/6)\r\n', 'val 1 = 7\r\nvaleur 2 = 5', 'resultat = 12\r\n\r\n![](index.php?/attachments/get/1)\r\n', NULL, NULL, NULL, 0, 6, 0, NULL, 1531960293, 1, NULL, NULL, 3, 1531990455, 1, 1),
(2, 1, 'negatif + negatif', 2, 2, NULL, NULL, 'Les champs sont vides\r\n\r\n![](index.php?/attachments/get/5)\r\n', 'val 1 = -3\r\nvaleur 2 = -2', 'résultat = -5\r\n\r\n![](index.php?/attachments/get/2)\r\n', NULL, NULL, NULL, 0, 6, 0, NULL, 1531960434, 1, NULL, NULL, 3, 1531990477, 1, 1),
(3, 1, 'positif + negatif', 3, 2, NULL, NULL, 'Les champs sont vides\r\n\r\n![](index.php?/attachments/get/4)\r\n', 'val 1 = 7\r\nvaleur 2 = -5', 'résultat = 2\r\n\r\n![](index.php?/attachments/get/3)\r\n', NULL, NULL, NULL, 0, 6, 0, NULL, 1531960566, 1, NULL, NULL, 3, 1531990519, 1, 1),
(4, 3, 'Test discriminant négatif', 3, 3, NULL, NULL, 'Vider les champs de text\r\n![](index.php?/attachments/get/13)\r\n', 'Saisir dans les champs dans cet ordre: -1, 0, -1\r\n![](index.php?/attachments/get/14)\r\nAppuyer sur le bouton intitulé \"Résoudre\"', 'Le système n\'admet pas de solutions dans R\r\n![](index.php?/attachments/get/15)\r\n', NULL, NULL, NULL, 0, 6, 0, NULL, 1533546011, 1, NULL, NULL, 4, 1533549964, 1, 1),
(5, 3, 'Test discriminant positif', 2, 2, NULL, NULL, 'Vider les champs de text\r\n![](index.php?/attachments/get/37)\r\n\r\n', 'Saisir dans les champs dans cet ordre: 1, 0, -1\r\n![](index.php?/attachments/get/17)\r\nAppuyer sur le bouton intitulé \"Résoudre\"', 'Les solutions sont x1: -1.0 x2: 1.0 S = {-1.0 , 1.0}\r\n![](index.php?/attachments/get/18)\r\n', NULL, NULL, NULL, 0, 6, 0, NULL, 1533547660, 1, NULL, NULL, 4, 1533626528, 1, 1),
(6, 3, 'Test discriminant nul', 1, 2, NULL, NULL, 'Vider les champs de text\r\n![](index.php?/attachments/get/19)\r\n', 'Saisir dans les champs dans cet ordre: 1, 2, 1\r\n![](index.php?/attachments/get/20)\r\nAppuyer sur le bouton intitulé \"Résoudre\"\r\n', 'Le système admet une solution double x0 :-1.0 S = {-1.0}\r\n![](index.php?/attachments/get/21)\r\n', NULL, NULL, NULL, 0, 6, 0, NULL, 1533547912, 1, NULL, NULL, 4, 1533626514, 1, 1),
(7, 4, 'Test champs vide', 3, 2, NULL, NULL, 'Vider les champs de text\r\n![](index.php?/attachments/get/22)\r\n', 'Appuyer sur le bouton intitulé \"Résoudre\"', 'Erreur: Veuiller saisir des nombres ou vérifier que tous les champs sont remplis\r\n![](index.php?/attachments/get/23)\r\n', NULL, NULL, NULL, 0, 7, 0, NULL, 1533548209, 1, NULL, NULL, 4, 1533550059, 1, 1),
(8, 4, 'Test \'a\' nul', 1, 2, NULL, NULL, 'Vider les champs de text\r\n![](index.php?/attachments/get/24)\r\n', 'Saisir dans les champs dans cet ordre: 0, 2, 1\r\n![](index.php?/attachments/get/25)\r\nAppuyer sur le bouton intitulé \"Résoudre\"', 'la valeur de a ne doit pas être nulle\r\n![](index.php?/attachments/get/26)\r\n', NULL, NULL, NULL, 0, 7, 0, NULL, 1533549066, 1, NULL, NULL, 4, 1533626632, 1, 1),
(9, 4, 'Test insertion de caractere', 2, 2, NULL, NULL, 'Vider les champs de text\r\n![](index.php?/attachments/get/27)\r\n', 'Saisir dans les champs dans cet ordre: a, 1s, 2\r\n![](index.php?/attachments/get/28)\r\nAppuyer sur le bouton intitulé \"Résoudre\"', 'Erreur: Veuiller saisir des nombres ou vérifier que tous les champs sont remplis\r\n![](index.php?/attachments/get/29)\r\n', NULL, NULL, NULL, 0, 7, 0, NULL, 1533549363, 1, NULL, NULL, 4, 1533550073, 1, 1),
(10, 3, 'Test de l\'historique', 4, 2, NULL, NULL, NULL, 'Appuyer sur le bouton de menu déroulant\r\nSélectionner \"Historique\"', 'Liste des équations précédemment résolues', NULL, NULL, NULL, 0, 6, 0, NULL, 1533626459, 1, NULL, NULL, 4, 1533626495, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `case_assocs`
--

CREATE TABLE `case_assocs` (
  `id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `case_changes`
--

CREATE TABLE `case_changes` (
  `id` int(11) NOT NULL,
  `case_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `changes` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `case_changes`
--

INSERT INTO `case_changes` (`id`, `case_id`, `type_id`, `created_on`, `user_id`, `changes`) VALUES
(1, 3, 6, 1531960652, 1, '[{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Preconditions\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_preconds\",\"old_value\":\"Les champs sont vides\",\"new_value\":\"Les champs sont vides\\r\\n\\r\\n![](index.php?\\/attachments\\/get\\/4)\\r\\n\"}]'),
(2, 3, 6, 1531960703, 1, '[{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Steps\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_steps\",\"old_value\":\"a = 7\\r\\nb = -5\",\"new_value\":\"val 1 = 7\\r\\nvaleur 2 = -5\"}]'),
(3, 3, 6, 1531960725, 1, '[{\"type_id\":1,\"field\":\"title\",\"old_value\":\"positif + negqtif\",\"new_value\":\"positif + negatif\"}]'),
(4, 2, 6, 1531960776, 1, '[{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Preconditions\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_preconds\",\"old_value\":\"Les champs sont vides\",\"new_value\":\"Les champs sont vides\\r\\n\\r\\n![](index.php?\\/attachments\\/get\\/5)\\r\\n\"},{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Steps\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_steps\",\"old_value\":\"a = -3\\r\\nb = -2\",\"new_value\":\"val 1 = -3\\r\\nvaleur 2 = -2\"}]'),
(5, 1, 6, 1531960823, 1, '[{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Preconditions\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_preconds\",\"old_value\":\"les champs sont vides\",\"new_value\":\"les champs sont vides\\r\\n\\r\\n![](index.php?\\/attachments\\/get\\/6)\\r\\n\"},{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Steps\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_steps\",\"old_value\":\"a = 7\\r\\nb = 5\",\"new_value\":\"val 1 = 7\\r\\nvaleur 2 = 5\"}]'),
(6, 1, 6, 1531972915, 2, '[{\"type_id\":1,\"field\":\"title\",\"old_value\":\"positif + positif\",\"new_value\":\"deux nombres positifs\"}]'),
(7, 2, 6, 1531972931, 2, '[{\"type_id\":1,\"field\":\"title\",\"old_value\":\"negatif + negatif\",\"new_value\":\"deux nombres negatifs\"}]'),
(8, 3, 6, 1531972940, 2, '[{\"type_id\":1,\"field\":\"title\",\"old_value\":\"positif + negatif\",\"new_value\":\"positif et negatif\"}]'),
(9, 1, 6, 1531990455, 1, '[{\"type_id\":1,\"field\":\"title\",\"old_value\":\"deux nombres positifs\",\"new_value\":\"positif + positif\"}]'),
(10, 2, 6, 1531990477, 1, '[{\"type_id\":1,\"field\":\"title\",\"old_value\":\"deux nombres negatifs\",\"new_value\":\"negatif + negatif\"}]'),
(11, 3, 6, 1531990519, 1, '[{\"type_id\":1,\"field\":\"title\",\"old_value\":\"positif et negatif\",\"new_value\":\"positif + negatif\"}]'),
(12, 5, 6, 1533548259, 1, '[{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Steps\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_steps\",\"old_value\":\"Saisir dans les champs dans cet ordre: 1, 0, -1\\r\\n![](index.php?\\/attachments\\/get\\/17)\\r\\n\",\"new_value\":\"Saisir dans les champs dans cet ordre: 1, 0, -1\\r\\n![](index.php?\\/attachments\\/get\\/17)\\r\\nAppuyer sur le bouton intitul\\u00e9 \\\"R\\u00e9soudre\\\"\"}]'),
(13, 6, 6, 1533548295, 1, '[{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Steps\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_steps\",\"old_value\":\"Saisir dans les champs dans cet ordre: 1, 2, 1\\r\\n![](index.php?\\/attachments\\/get\\/20)\\r\\n\",\"new_value\":\"Saisir dans les champs dans cet ordre: 1, 2, 1\\r\\n![](index.php?\\/attachments\\/get\\/20)\\r\\nAppuyer sur le bouton intitul\\u00e9 \\\"R\\u00e9soudre\\\"\\r\\n\"}]'),
(14, 8, 6, 1533549484, 1, '[{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Steps\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_steps\",\"old_value\":\"Saisir dans les champs dans cet ordre: 0, 2, 1\\r\\n![](index.php?\\/attachments\\/get\\/25)\\r\\n\",\"new_value\":\"Saisir dans les champs dans cet ordre: 0, 2, 1\\r\\n![](index.php?\\/attachments\\/get\\/25)\\r\\nAppuyer sur le bouton intitul\\u00e9 \\\"R\\u00e9soudre\\\"\"}]'),
(15, 4, 6, 1533549964, 1, '[{\"type_id\":1,\"old_text\":\"Test Cases\",\"new_text\":\"Evaluation de la completude\",\"field\":\"section_id\",\"old_value\":2,\"new_value\":3}]'),
(16, 5, 6, 1533549971, 1, '[{\"type_id\":1,\"old_text\":\"Test Cases\",\"new_text\":\"Evaluation de la completude\",\"field\":\"section_id\",\"old_value\":2,\"new_value\":3}]'),
(17, 6, 6, 1533549978, 1, '[{\"type_id\":1,\"old_text\":\"Test Cases\",\"new_text\":\"Evaluation de la completude\",\"field\":\"section_id\",\"old_value\":2,\"new_value\":3}]'),
(18, 7, 6, 1533550059, 1, '[{\"type_id\":1,\"old_text\":\"Test Cases\",\"new_text\":\"Evaluation de la protection d\'acces\",\"field\":\"section_id\",\"old_value\":2,\"new_value\":4}]'),
(19, 8, 6, 1533550066, 1, '[{\"type_id\":1,\"old_text\":\"Test Cases\",\"new_text\":\"Evaluation de la protection d\'acces\",\"field\":\"section_id\",\"old_value\":2,\"new_value\":4}]'),
(20, 9, 6, 1533550073, 1, '[{\"type_id\":1,\"old_text\":\"Test Cases\",\"new_text\":\"Evaluation de la protection d\'acces\",\"field\":\"section_id\",\"old_value\":2,\"new_value\":4}]'),
(21, 5, 6, 1533553565, 1, '[{\"type_id\":6,\"old_text\":null,\"new_text\":null,\"label\":\"Preconditions\",\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"field\":\"custom_preconds\",\"old_value\":\"Vider les champs de text\\r\\n![](index.php?\\/attachments\\/get\\/16)\\r\\n\",\"new_value\":\"Vider les champs de text\\r\\n![](index.php?\\/attachments\\/get\\/37)\\r\\n\\r\\n\"}]'),
(22, 10, 6, 1533626496, 1, '[{\"type_id\":1,\"old_text\":\"Test Cases\",\"new_text\":\"Tests de fonctionnalit\\u00e9\",\"field\":\"section_id\",\"old_value\":2,\"new_value\":3}]'),
(23, 6, 6, 1533626514, 1, '[{\"type_id\":1,\"old_text\":\"Other\",\"new_text\":\"Functional\",\"field\":\"type_id\",\"old_value\":7,\"new_value\":6}]'),
(24, 5, 6, 1533626528, 1, '[{\"type_id\":1,\"old_text\":\"Other\",\"new_text\":\"Functional\",\"field\":\"type_id\",\"old_value\":7,\"new_value\":6}]');

-- --------------------------------------------------------

--
-- Structure de la table `case_types`
--

CREATE TABLE `case_types` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `case_types`
--

INSERT INTO `case_types` (`id`, `name`, `is_default`, `is_deleted`) VALUES
(1, 'Acceptance', 0, 0),
(2, 'Accessibility', 0, 0),
(3, 'Automated', 0, 0),
(4, 'Compatibility', 0, 0),
(5, 'Destructive', 0, 0),
(6, 'Functional', 0, 0),
(7, 'Other', 1, 0),
(8, 'Performance', 0, 0),
(9, 'Regression', 0, 0),
(10, 'Security', 0, 0),
(11, 'Smoke & Sanity', 0, 0),
(12, 'Usability', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `configs`
--

CREATE TABLE `configs` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `config_groups`
--

CREATE TABLE `config_groups` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `defects`
--

CREATE TABLE `defects` (
  `id` int(11) NOT NULL,
  `defect_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `test_change_id` int(11) NOT NULL,
  `case_id` int(11) DEFAULT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `exports`
--

CREATE TABLE `exports` (
  `id` int(11) NOT NULL,
  `filename` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `size` bigint(20) NOT NULL,
  `created_on` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fields`
--

CREATE TABLE `fields` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `system_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `entity_id` int(11) NOT NULL,
  `label` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `type_id` int(11) NOT NULL,
  `location_id` int(11) NOT NULL,
  `display_order` int(11) NOT NULL,
  `configs` longtext COLLATE utf8_unicode_ci NOT NULL,
  `is_multi` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `status_id` int(11) NOT NULL,
  `is_system` tinyint(1) NOT NULL,
  `include_all` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `fields`
--

INSERT INTO `fields` (`id`, `name`, `system_name`, `entity_id`, `label`, `description`, `type_id`, `location_id`, `display_order`, `configs`, `is_multi`, `is_active`, `status_id`, `is_system`, `include_all`) VALUES
(1, 'preconds', 'custom_preconds', 1, 'Preconditions', 'The preconditions of this test case. Reference other test cases with [C#] (e.g. [C17]).', 3, 2, 1, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"id\":\"4be1344d55d11\"}]', 0, 1, 1, 0, 0),
(2, 'steps', 'custom_steps', 1, 'Steps', 'The required steps to execute the test case.', 3, 2, 2, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"id\":\"4be97c65ea2fd\"}]', 0, 1, 1, 0, 0),
(3, 'expected', 'custom_expected', 1, 'Expected Result', 'The expected result after executing the test case.', 3, 2, 3, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"id\":\"4be1345cafd07\"}]', 0, 1, 1, 0, 0),
(4, 'dc330d77', 'estimate', 1, 'Estimate', NULL, 1, 1, 1, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false},\"id\":\"4be97c65ea2fd\"}]', 0, 1, 1, 1, 1),
(5, 'ddfe71c8', 'milestone_id', 1, 'Milestone', NULL, 9, 1, 2, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false},\"id\":\"4be97c65ea2fd\"}]', 0, 0, 1, 1, 1),
(6, 'c4bd4336', 'refs', 1, 'References', NULL, 1, 1, 3, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false},\"id\":\"4be97c65ea2fd\"}]', 0, 1, 1, 1, 1),
(7, 'd4d1e651', 'version', 2, 'Version', NULL, 1, 4, 2, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false},\"id\":\"4be97c65ea2fd\"}]', 0, 1, 1, 1, 1),
(8, 'e7c13ac2', 'elapsed', 2, 'Elapsed', NULL, 1, 4, 3, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false},\"id\":\"4be97c65ea2fd\"}]', 0, 1, 1, 1, 1),
(9, 'a6637b4f', 'defects', 2, 'Defects', NULL, 1, 4, 4, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false},\"id\":\"4be97c65ea2fd\"}]', 0, 1, 1, 1, 1),
(10, 'steps_separated', 'custom_steps_separated', 1, 'Steps', NULL, 10, 2, 4, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false,\"format\":\"markdown\",\"has_expected\":true,\"rows\":\"5\"},\"id\":\"4be97c65ea2fd\"}]', 0, 1, 1, 0, 0),
(11, 'step_results', 'custom_step_results', 2, 'Steps', NULL, 11, 3, 1, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false,\"format\":\"markdown\",\"has_expected\":true,\"has_actual\":true,\"rows\":\"5\"},\"id\":\"4be97c65ea2fd\"}]', 0, 1, 1, 0, 0),
(12, 'mission', 'custom_mission', 1, 'Mission', 'A high-level overview of what to test and which areas to cover, usually just 1-2 sentences.', 3, 2, 5, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"id\":\"4be1345cafd07\"}]', 0, 1, 1, 0, 0),
(13, 'goals', 'custom_goals', 1, 'Goals', 'A detailed list of goals to cover as part of the exploratory sessions.', 3, 2, 6, '[{\"context\":{\"is_global\":true,\"project_ids\":null},\"options\":{\"is_required\":false,\"default_value\":\"\",\"format\":\"markdown\",\"rows\":\"7\"},\"id\":\"4be1345cafd07\"}]', 0, 1, 1, 0, 0),
(14, 'automation_type', 'custom_automation_type', 1, 'Automation Type', NULL, 6, 1, 5, '[{\"context\":{\"is_global\":true,\"project_ids\":[]},\"options\":{\"is_required\":false,\"default_value\":\"0\",\"items\":\"0, None\\n1, Ranorex\"},\"id\":\"7a34a519-f458-40bb-af43-ed63baf874ee\"}]', 0, 1, 1, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `field_templates`
--

CREATE TABLE `field_templates` (
  `field_id` int(11) NOT NULL,
  `template_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `field_templates`
--

INSERT INTO `field_templates` (`field_id`, `template_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(3, 1),
(10, 2),
(11, 2),
(12, 3),
(13, 3);

-- --------------------------------------------------------

--
-- Structure de la table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `group_users`
--

CREATE TABLE `group_users` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `heartbeat` int(11) NOT NULL,
  `is_done` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `jobs`
--

INSERT INTO `jobs` (`id`, `name`, `created_on`, `is_locked`, `heartbeat`, `is_done`) VALUES
(1, 'check_for_update', 1531939376, 1, 1533611594, 0),
(2, 'defect_gc', 1531939377, 0, 1533317162, 0),
(3, 'fields', 1531939378, 0, 1533629221, 0),
(4, 'forecasts', 1531939378, 0, 1533626043, 0),
(5, 'migration_160', 1531939378, 0, 1531939378, 1),
(6, 'migration_161', 1531939378, 0, 1531939379, 1),
(7, 'notifications', 1531939379, 0, 1533629221, 0),
(8, 'progress', 1531939379, 0, 1533625922, 0),
(9, 'reference_gc', 1531939379, 0, 1533317162, 0),
(10, 'report_jobs', 1531939379, 0, 1533629221, 0),
(11, 'reports', 1531939379, 0, 0, 0),
(12, 'session_gc', 1531939379, 0, 1533626044, 0),
(13, 'token_gc', 1531939380, 0, 1533626044, 0);

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `subject` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `message_recps`
--

CREATE TABLE `message_recps` (
  `user_id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `milestones`
--

CREATE TABLE `milestones` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `due_on` int(11) DEFAULT NULL,
  `completed_on` int(11) DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `start_on` int(11) DEFAULT NULL,
  `started_on` int(11) DEFAULT NULL,
  `is_started` tinyint(1) NOT NULL,
  `parent_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `preferences`
--

CREATE TABLE `preferences` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `preferences`
--

INSERT INTO `preferences` (`id`, `user_id`, `name`, `value`) VALUES
(1, 1, 'goals', '{\"1\":true,\"3\":true,\"2\":true,\"4\":true,\"5\":true,\"6\":true}'),
(3, 1, 'todos_overview_user_ids', '1'),
(4, 1, 'todos_overview_status_ids', '3,4,5'),
(7, 1, 'tests_qpane', '1'),
(8, 1, 'cases_qpane', '1'),
(9, 1, 'admin_users_show', 'active'),
(12, 1, 'goals_finished', '1'),
(13, 2, 'tests_qpane', '0');

-- --------------------------------------------------------

--
-- Structure de la table `priorities`
--

CREATE TABLE `priorities` (
  `id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `short_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `priorities`
--

INSERT INTO `priorities` (`id`, `priority`, `name`, `short_name`, `is_default`, `is_deleted`) VALUES
(1, 1, 'Low', 'Low', 0, 0),
(2, 2, 'Medium', 'Medium', 1, 0),
(3, 3, 'High', 'High', 0, 0),
(4, 4, 'Critical', 'Critical', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `announcement` longtext COLLATE utf8_unicode_ci,
  `show_announcement` tinyint(1) NOT NULL,
  `defect_id_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defect_add_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_access` int(11) NOT NULL,
  `default_role_id` int(11) DEFAULT NULL,
  `reference_id_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_add_url` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defect_plugin` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defect_config` longtext COLLATE utf8_unicode_ci,
  `is_completed` tinyint(1) NOT NULL,
  `completed_on` int(11) DEFAULT NULL,
  `defect_template` longtext COLLATE utf8_unicode_ci,
  `suite_mode` int(11) NOT NULL,
  `master_id` int(11) DEFAULT NULL,
  `reference_plugin` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_config` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `projects`
--

INSERT INTO `projects` (`id`, `name`, `announcement`, `show_announcement`, `defect_id_url`, `defect_add_url`, `default_access`, `default_role_id`, `reference_id_url`, `reference_add_url`, `defect_plugin`, `defect_config`, `is_completed`, `completed_on`, `defect_template`, `suite_mode`, `master_id`, `reference_plugin`, `reference_config`) VALUES
(3, 'somme', NULL, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 3, NULL, NULL),
(4, 'OperationR', NULL, 0, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, 1, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `project_access`
--

CREATE TABLE `project_access` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_favs`
--

CREATE TABLE `project_favs` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_on` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_groups`
--

CREATE TABLE `project_groups` (
  `project_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `access` int(11) NOT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `project_history`
--

CREATE TABLE `project_history` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `action` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `suite_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `run_id` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `plan_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `project_history`
--

INSERT INTO `project_history` (`id`, `project_id`, `action`, `created_on`, `user_id`, `suite_id`, `milestone_id`, `run_id`, `name`, `is_deleted`, `plan_id`) VALUES
(1, 3, 1, 1531948891, 1, NULL, NULL, 1, 'Test Run 18/07/2018', 1, NULL),
(2, 3, 2, 1531960957, 1, NULL, NULL, 1, 'Test Run 18/07/2018', 1, NULL),
(3, 3, 1, 1531960974, 1, NULL, NULL, 2, NULL, 0, NULL),
(4, 3, 1, 1531973010, 2, NULL, NULL, 3, NULL, 0, NULL),
(5, 4, 1, 1533550234, 1, NULL, NULL, 4, NULL, 0, NULL),
(6, 4, 1, 1533626471, 1, NULL, NULL, 5, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `refs`
--

CREATE TABLE `refs` (
  `id` int(11) NOT NULL,
  `reference_id` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `case_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `reports`
--

CREATE TABLE `reports` (
  `id` int(11) NOT NULL,
  `plugin` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `access` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `executed_on` int(11) DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  `dir` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formats` longtext COLLATE utf8_unicode_ci NOT NULL,
  `system_options` longtext COLLATE utf8_unicode_ci,
  `custom_options` longtext COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL,
  `status_message` longtext COLLATE utf8_unicode_ci,
  `status_trace` longtext COLLATE utf8_unicode_ci,
  `is_locked` tinyint(1) NOT NULL,
  `heartbeat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `reports`
--

INSERT INTO `reports` (`id`, `plugin`, `project_id`, `name`, `description`, `access`, `created_by`, `created_on`, `executed_on`, `execution_time`, `dir`, `formats`, `system_options`, `custom_options`, `status`, `status_message`, `status_trace`, `is_locked`, `heartbeat`) VALUES
(3, 'runs_summary', 3, 'Runs (Summary) 19/07/2018', NULL, 1, 1, 1531962331, 1531962366, 3975, '2018/07/report-3-00f500d3-0fab-48de-9e09-041c3751ca1f', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Runs (Summary) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"runs_suites_include\":\"1\",\"runs_suites_ids\":null,\"runs_filters\":null,\"runs_include\":\"1\",\"runs_ids\":null,\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":25,\"activities_daterange\":\"5\",\"activities_daterange_from\":null,\"activities_daterange_to\":null,\"activities_statuses_include\":\"1\",\"activities_statuses_ids\":null,\"activities_limit\":100,\"tests_filters\":null,\"tests_columns\":{\"tests:id\":75,\"cases:title\":0},\"tests_limit\":100,\"content_hide_links\":false,\"status_include\":true,\"activities_include\":true,\"progress_include\":true,\"tests_include\":true}', 2, NULL, NULL, 0, 1531962366),
(4, 'cases_activity_summary', 3, 'Activity Summary (Cases) 19/07/2018', NULL, 1, 1, 1531962433, 1531962483, 286, '2018/07/report-4-ccc1f84e-2d7f-4a4f-9874-bb268c13e7e4', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Activity Summary (Cases) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"cases_groupby\":\"day\",\"changes_daterange\":\"5\",\"changes_daterange_from\":null,\"changes_daterange_to\":null,\"suites_include\":\"1\",\"suites_ids\":null,\"sections_include\":\"1\",\"sections_ids\":null,\"cases_columns\":{\"cases:id\":75,\"cases:title\":0,\"cases:created_by\":125,\"cases:updated_by\":125},\"cases_filters\":null,\"cases_limit\":1000,\"content_hide_links\":false,\"cases_include_new\":true,\"cases_include_updated\":true}', 2, NULL, NULL, 0, 1531962483),
(5, 'cases_defect_summary', 3, 'Summary for Cases (Defects) 19/07/2018', NULL, 1, 1, 1531962787, 1531962842, 391, '2018/07/report-5-b74a7136-ef66-48d8-8267-9dfd343b9780', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Summary for Cases (Defects) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"runs_suites_id\":3,\"runs_filters\":null,\"runs_include\":\"1\",\"runs_ids\":null,\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":10,\"cases_columns\":{\"cases:id\":75,\"cases:title\":0},\"cases_filters\":null,\"cases_limit\":1000,\"content_hide_links\":false,\"cases_include_comparison\":true,\"cases_include_summary\":true}', 2, NULL, NULL, 0, 1531962842),
(6, 'runs_summary', 3, 'Runs (Summary) 19/07/2018', NULL, 1, 2, 1531973177, 1531973223, 562, '2018/07/report-6-c4328d88-e2e0-4c72-9b1c-b1d77a55ff70', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Runs (Summary) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"runs_suites_include\":\"1\",\"runs_suites_ids\":null,\"runs_filters\":null,\"runs_include\":\"3\",\"runs_ids\":[3],\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":25,\"activities_daterange\":\"5\",\"activities_daterange_from\":null,\"activities_daterange_to\":null,\"activities_statuses_include\":\"1\",\"activities_statuses_ids\":null,\"activities_limit\":100,\"tests_filters\":null,\"tests_columns\":{\"tests:id\":75,\"cases:title\":0},\"tests_limit\":100,\"content_hide_links\":false,\"status_include\":true,\"activities_include\":true,\"progress_include\":true,\"tests_include\":true}', 2, NULL, NULL, 0, 1531973223),
(7, 'runs_summary', 3, 'Runs (Summary) 19/07/2018', NULL, 1, 1, 1531990588, 1531990622, 478, '2018/07/report-7-252537b8-a3a7-4f76-9511-b147e94451ee', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Runs (Summary) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"runs_suites_include\":\"1\",\"runs_suites_ids\":null,\"runs_filters\":null,\"runs_include\":\"1\",\"runs_ids\":[2],\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":25,\"activities_daterange\":\"5\",\"activities_daterange_from\":null,\"activities_daterange_to\":null,\"activities_statuses_include\":\"1\",\"activities_statuses_ids\":null,\"activities_limit\":100,\"tests_filters\":null,\"tests_columns\":{\"tests:id\":75,\"cases:title\":0},\"tests_limit\":100,\"content_hide_links\":false,\"status_include\":true,\"activities_include\":true,\"progress_include\":true,\"tests_include\":true}', 2, NULL, NULL, 0, 1531990622),
(8, 'runs_summary', 4, 'Runs (Summary) 06/08/2018', NULL, 1, 1, 1533551428, 1533551467, 4266, '2018/08/report-8-e9a31937-35f6-43f5-ba06-e60c64270252', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Runs (Summary) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"runs_suites_include\":\"1\",\"runs_suites_ids\":null,\"runs_filters\":null,\"runs_include\":\"1\",\"runs_ids\":null,\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":25,\"activities_daterange\":\"5\",\"activities_daterange_from\":null,\"activities_daterange_to\":null,\"activities_statuses_include\":\"1\",\"activities_statuses_ids\":null,\"activities_limit\":100,\"tests_filters\":null,\"tests_columns\":{\"tests:id\":75,\"cases:title\":0},\"tests_limit\":100,\"content_hide_links\":false,\"status_include\":true,\"activities_include\":true,\"progress_include\":true,\"tests_include\":true}', 2, NULL, NULL, 0, 1533551467),
(9, 'cases_defect_summary', 4, 'Summary for Cases (Defects) 06/08/2018', NULL, 1, 1, 1533557141, 1533557163, 480, '2018/08/report-9-e9641cc5-3974-48b6-8e8e-febf6e2bfa18', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Summary for Cases (Defects) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"runs_suites_id\":4,\"runs_filters\":null,\"runs_include\":\"1\",\"runs_ids\":null,\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":10,\"cases_columns\":{\"cases:id\":75,\"cases:title\":0},\"cases_filters\":null,\"cases_limit\":1000,\"content_hide_links\":false,\"cases_include_comparison\":true,\"cases_include_summary\":true}', 2, NULL, NULL, 0, 1533557163),
(10, 'cases_result_coverage', 4, 'Comparison for Cases (Results) 06/08/2018', NULL, 1, 1, 1533557172, 1533557222, 575, '2018/08/report-10-4a838677-d87e-45d3-ad2e-430884c307a2', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Comparison for Cases (Results) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"runs_suites_id\":4,\"runs_filters\":null,\"runs_include\":\"1\",\"runs_ids\":null,\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":10,\"cases_columns\":{\"cases:id\":75,\"cases:title\":0},\"cases_filters\":null,\"cases_limit\":1000,\"content_hide_links\":false,\"cases_include_comparison\":true,\"cases_include_coverage\":true}', 2, NULL, NULL, 0, 1533557222),
(11, 'projects_summary', 4, 'Project (Summary) 06/08/2018', NULL, 1, 1, 1533557190, 1533557223, 409, '2018/08/report-11-b9b7942a-1329-457d-af50-4570506405eb', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Project (Summary) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"milestones_completed_limit\":10,\"runs_completed_limit\":10,\"history_daterange\":\"5\",\"history_daterange_from\":null,\"history_daterange_to\":null,\"history_limit\":100,\"activities_daterange\":\"5\",\"activities_daterange_from\":null,\"activities_daterange_to\":null,\"activities_statuses_include\":\"1\",\"activities_statuses_ids\":null,\"activities_limit\":100,\"content_hide_links\":false,\"milestones_active_include\":true,\"milestones_completed_include\":false,\"runs_active_include\":true,\"runs_completed_include\":false,\"activities_include\":true,\"history_include\":true}', 2, NULL, NULL, 0, 1533557223),
(12, 'defects_summary', 4, 'Summary (Defects) 07/08/2018', NULL, 1, 1, 1533628922, 1533628987, 5058, '2018/08/report-12-e65924bc-df23-41c4-bbac-b16f0732860e', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Summary (Defects) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"defects_include\":\"1\",\"defects_ids\":null,\"runs_suites_include\":\"1\",\"runs_suites_ids\":null,\"runs_filters\":null,\"runs_include\":\"1\",\"runs_ids\":null,\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":25,\"tests_columns\":{\"tests:id\":75,\"cases:title\":0},\"tests_filters\":null,\"tests_limit\":1000,\"content_hide_links\":false}', 2, NULL, NULL, 0, 1533628987),
(13, 'runs_summary', 4, 'Runs (Summary) 07/08/2018', NULL, 1, 1, 1533628946, 1533628988, 946, '2018/08/report-13-447b65cb-b993-48cb-921d-cb2d11965f72', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Runs (Summary) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"runs_suites_include\":\"1\",\"runs_suites_ids\":null,\"runs_filters\":null,\"runs_include\":\"1\",\"runs_ids\":null,\"runs_sections_include\":\"1\",\"runs_sections_ids\":null,\"runs_limit\":25,\"activities_daterange\":\"5\",\"activities_daterange_from\":null,\"activities_daterange_to\":null,\"activities_statuses_include\":\"1\",\"activities_statuses_ids\":null,\"activities_limit\":100,\"tests_filters\":null,\"tests_columns\":{\"tests:id\":75,\"cases:title\":0},\"tests_limit\":100,\"content_hide_links\":false,\"status_include\":true,\"activities_include\":true,\"progress_include\":true,\"tests_include\":true}', 2, NULL, NULL, 0, 1533628988),
(14, 'projects_summary', 4, 'Project (Summary) 07/08/2018', NULL, 1, 1, 1533628961, 1533628989, 580, '2018/08/report-14-aa5f06f9-f682-4c48-bb29-d53c46fb9ebb', '[\"raw_zip\",\"inline\",\"standalone_zip\",\"pdf\"]', '{\"name\":\"Project (Summary) %date%\",\"description\":null,\"access\":\"1\",\"schedule_now\":true,\"schedule_later\":false,\"schedule_interval\":\"1\",\"schedule_weekday\":1,\"schedule_day\":1,\"schedule_hour\":8,\"notify_user\":false,\"notify_link\":false,\"notify_link_recipients\":null,\"notify_attachment\":false,\"notify_attachment_recipients\":\"person1@example.com\\r\\nperson2@example.com\",\"notify_attachment_html_format\":false,\"notify_attachment_pdf_format\":false}', '{\"milestones_completed_limit\":10,\"runs_completed_limit\":10,\"history_daterange\":\"5\",\"history_daterange_from\":null,\"history_daterange_to\":null,\"history_limit\":100,\"activities_daterange\":\"5\",\"activities_daterange_from\":null,\"activities_daterange_to\":null,\"activities_statuses_include\":\"1\",\"activities_statuses_ids\":null,\"activities_limit\":100,\"content_hide_links\":false,\"milestones_active_include\":true,\"milestones_completed_include\":false,\"runs_active_include\":true,\"runs_completed_include\":false,\"activities_include\":true,\"history_include\":true}', 2, NULL, NULL, 0, 1533628989);

-- --------------------------------------------------------

--
-- Structure de la table `report_jobs`
--

CREATE TABLE `report_jobs` (
  `id` int(11) NOT NULL,
  `plugin` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_on` int(11) NOT NULL,
  `executed_on` int(11) DEFAULT NULL,
  `system_options` longtext COLLATE utf8_unicode_ci,
  `custom_options` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` int(11) NOT NULL,
  `is_default` int(11) NOT NULL,
  `display_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `roles`
--

INSERT INTO `roles` (`id`, `name`, `permissions`, `is_default`, `display_order`) VALUES
(1, 'Lead', 262143, 1, 18),
(2, 'Designer', 258636, 0, 10),
(3, 'Tester', 258624, 0, 8),
(4, 'Read-only', 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `runs`
--

CREATE TABLE `runs` (
  `id` int(11) NOT NULL,
  `suite_id` int(11) DEFAULT NULL,
  `milestone_id` int(11) DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `completed_on` int(11) DEFAULT NULL,
  `include_all` tinyint(1) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `passed_count` int(11) NOT NULL DEFAULT '0',
  `retest_count` int(11) NOT NULL DEFAULT '0',
  `failed_count` int(11) NOT NULL DEFAULT '0',
  `untested_count` int(11) NOT NULL DEFAULT '0',
  `assignedto_id` int(11) DEFAULT NULL,
  `is_plan` tinyint(1) NOT NULL DEFAULT '0',
  `plan_id` int(11) DEFAULT NULL,
  `entry_id` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entries` longtext COLLATE utf8_unicode_ci,
  `config` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `config_ids` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_index` int(11) DEFAULT NULL,
  `blocked_count` int(11) NOT NULL DEFAULT '0',
  `is_editable` tinyint(1) NOT NULL,
  `content_id` int(11) DEFAULT NULL,
  `custom_status1_count` int(11) NOT NULL DEFAULT '0',
  `custom_status2_count` int(11) NOT NULL DEFAULT '0',
  `custom_status3_count` int(11) NOT NULL DEFAULT '0',
  `custom_status4_count` int(11) NOT NULL DEFAULT '0',
  `custom_status5_count` int(11) NOT NULL DEFAULT '0',
  `custom_status6_count` int(11) NOT NULL DEFAULT '0',
  `custom_status7_count` int(11) NOT NULL DEFAULT '0',
  `updated_by` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `runs`
--

INSERT INTO `runs` (`id`, `suite_id`, `milestone_id`, `created_on`, `user_id`, `project_id`, `is_completed`, `completed_on`, `include_all`, `name`, `description`, `passed_count`, `retest_count`, `failed_count`, `untested_count`, `assignedto_id`, `is_plan`, `plan_id`, `entry_id`, `entries`, `config`, `config_ids`, `entry_index`, `blocked_count`, `is_editable`, `content_id`, `custom_status1_count`, `custom_status2_count`, `custom_status3_count`, `custom_status4_count`, `custom_status5_count`, `custom_status6_count`, `custom_status7_count`, `updated_by`, `updated_on`) VALUES
(2, 3, NULL, 1531960974, 1, 3, 0, NULL, 1, 'Test Run 19/07/2018', NULL, 3, 0, 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 1, 1531960974),
(3, 3, NULL, 1531973010, 2, 3, 0, NULL, 1, 'Test Run 19/07/2018', NULL, 3, 0, 0, 0, 2, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 3, 0, 0, 0, 0, 0, 0, 0, 2, 1531973010),
(4, 4, NULL, 1533550234, 1, 4, 0, NULL, 1, 'Test Run 06/08/2018', NULL, 6, 1, 0, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0, 1, 1533550234),
(5, 4, NULL, 1533626471, 1, 4, 0, NULL, 1, 'Test Run 07/08/2018', NULL, 6, 0, 1, 0, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 4, 0, 0, 0, 0, 0, 0, 0, 1, 1533626471);

-- --------------------------------------------------------

--
-- Structure de la table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `suite_id` int(11) DEFAULT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `display_order` int(11) NOT NULL,
  `is_copy` tinyint(1) NOT NULL,
  `copyof_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL DEFAULT '0',
  `description` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `sections`
--

INSERT INTO `sections` (`id`, `suite_id`, `name`, `display_order`, `is_copy`, `copyof_id`, `parent_id`, `depth`, `description`) VALUES
(1, 3, 'Test Cases', 1, 0, NULL, NULL, 0, NULL),
(2, 4, 'Test Cases', 1, 0, NULL, NULL, 0, NULL),
(3, 4, 'Tests de fonctionnalité', 2, 0, NULL, 2, 1, NULL),
(4, 4, 'Evaluation de la gestion des erreurs', 3, 0, NULL, 2, 1, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `user_agent` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `last_activity` int(10) NOT NULL,
  `user_data` longtext COLLATE utf8_unicode_ci,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `sessions`
--

INSERT INTO `sessions` (`session_id`, `ip_address`, `user_agent`, `last_activity`, `user_data`, `id`) VALUES
('eb8c135b-94b7-46aa-9faa-1072460d0539', '', '', 1533629101, '{\"last_login\":1533431432,\"user_id\":1,\"rememberme\":true,\"csrf\":\"3W8OL5gGE\\/bINsUAPy5f\",\"users_tab\":1}', 1);

-- --------------------------------------------------------

--
-- Structure de la table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `settings`
--

INSERT INTO `settings` (`id`, `name`, `value`) VALUES
(1, 'session_policy', ''),
(2, 'session_absolute_policy', ''),
(3, 'session_remember_me_disabled', '0'),
(4, 'database_version', '188'),
(5, 'installation_name', 'TestRail QA'),
(6, 'installation_url', 'http://www.testrail.test/'),
(7, 'attachment_dir', '/var/www/testrail/attachments/'),
(8, 'report_dir', '/var/www/testrail/reports/'),
(9, 'default_language', 'en'),
(10, 'default_locale', 'fr-fr'),
(11, 'default_timezone', 'Etc/UTC'),
(12, 'email_server', NULL),
(13, 'email_ssl', '0'),
(14, 'email_from', NULL),
(15, 'email_user', NULL),
(16, 'email_pass', NULL),
(17, 'email_notifications', '1'),
(18, 'license_key', 'QuZCAYLI6iNkSDdAITRnii7YMLtFPUjtXJAulyfe7GWNFBsHZJl7ro7t0qxw\r\nt7JX0fBfQconpSLPe3mYCFLQuBeLTqaHyoeCm1qhSDAZPPaQz45ZrdsBH0o0\r\nI+GUMPIap9MMkP6IWjiuyNkwVrQi85aBB9QP0Wp0Qwz9GxRHGl9sGNShF1xT\r\nYc1QEZ6xjcHaW37+h5e+GHamH5QEQylAoUpIMhrh1WUtdMWrJosuyRizVg51\r\nTfRKa9OXZmzrtUfW'),
(19, 'latest_version', '5.5.1.3746'),
(32, 'login_text', NULL),
(33, 'password_policy', NULL),
(34, 'password_policy_custom', '.{15,}\r\n[a-z]\r\n[A-Z]\r\n[0-9]\r\n[`~!@#$%^&*()\\-_=+[\\]|;:\'\",<>./?]'),
(35, 'password_policy_desc', 'Minimum of 15 characters, at least one lower & upper case character, a number and a special character.'),
(36, 'forgot_password', '1'),
(37, 'invite_users', '1'),
(38, 'ip_check', '0'),
(39, 'ip_policy', '; You can use simple IP addresses:\r\n; 192.168.1.1\r\n; Or entire networks:\r\n; 192.168.1.0/24'),
(40, 'check_for_updates', '1'),
(41, 'edit_mode', '86400'),
(42, 'name_format', '0'),
(43, 'partial_count', '500'),
(44, 'apiv2_enabled', '1'),
(45, 'apiv2_session_enabled', '1');

-- --------------------------------------------------------

--
-- Structure de la table `statuses`
--

CREATE TABLE `statuses` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `system_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `color_dark` int(11) NOT NULL,
  `color_medium` int(11) NOT NULL,
  `color_bright` int(11) NOT NULL,
  `display_order` int(11) NOT NULL,
  `is_system` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `is_untested` tinyint(1) NOT NULL,
  `is_final` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `statuses`
--

INSERT INTO `statuses` (`id`, `name`, `system_name`, `label`, `color_dark`, `color_medium`, `color_bright`, `display_order`, `is_system`, `is_active`, `is_untested`, `is_final`) VALUES
(1, 'passed', 'passed', 'Passed', 6667107, 9820525, 12709313, 1, 1, 1, 0, 1),
(2, 'blocked', 'blocked', 'Blocked', 9474192, 13684944, 14737632, 2, 1, 1, 0, 1),
(3, 'untested', 'untested', 'Untested', 11579568, 15395562, 15790320, 3, 1, 1, 1, 0),
(4, 'retest', 'retest', 'Retest', 13026868, 15593088, 16448182, 4, 1, 1, 0, 0),
(5, 'failed', 'failed', 'Failed', 14250867, 15829135, 16631751, 5, 1, 1, 0, 1),
(6, 'custom_status1', 'custom_status1', 'Unnamed 1', 0, 10526880, 13684944, 6, 0, 0, 0, 0),
(7, 'custom_status2', 'custom_status2', 'Unnamed 2', 0, 10526880, 13684944, 7, 0, 0, 0, 0),
(8, 'custom_status3', 'custom_status3', 'Unnamed 3', 0, 10526880, 13684944, 8, 0, 0, 0, 0),
(9, 'custom_status4', 'custom_status4', 'Unnamed 4', 0, 10526880, 13684944, 9, 0, 0, 0, 0),
(10, 'custom_status5', 'custom_status5', 'Unnamed 5', 0, 10526880, 13684944, 10, 0, 0, 0, 0),
(11, 'custom_status6', 'custom_status6', 'Unnamed 6', 0, 10526880, 13684944, 11, 0, 0, 0, 0),
(12, 'custom_status7', 'custom_status7', 'Unnamed 7', 0, 10526880, 13684944, 12, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `subscriptions`
--

CREATE TABLE `subscriptions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `is_subscribed` tinyint(1) NOT NULL,
  `test_id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `suites`
--

CREATE TABLE `suites` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `created_on` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_copy` tinyint(1) NOT NULL,
  `copyof_id` int(11) DEFAULT NULL,
  `is_master` tinyint(1) NOT NULL,
  `is_baseline` tinyint(1) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `completed_on` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `suites`
--

INSERT INTO `suites` (`id`, `name`, `project_id`, `description`, `created_on`, `created_by`, `is_copy`, `copyof_id`, `is_master`, `is_baseline`, `parent_id`, `is_completed`, `completed_on`) VALUES
(3, 'Master', 3, NULL, 1531948856, 1, 0, NULL, 1, 0, NULL, 0, NULL),
(4, 'Master', 4, NULL, 1533545229, 1, 0, NULL, 1, 0, NULL, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `is_locked` tinyint(1) NOT NULL,
  `heartbeat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `task`
--

INSERT INTO `task` (`id`, `is_locked`, `heartbeat`) VALUES
(1, 0, 1533629222);

-- --------------------------------------------------------

--
-- Structure de la table `templates`
--

CREATE TABLE `templates` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `include_all` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `templates`
--

INSERT INTO `templates` (`id`, `name`, `is_default`, `is_deleted`, `include_all`) VALUES
(1, 'Test Case (Text)', 1, 0, 1),
(2, 'Test Case (Steps)', 0, 0, 1),
(3, 'Exploratory Session', 0, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `template_projects`
--

CREATE TABLE `template_projects` (
  `template_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tests`
--

CREATE TABLE `tests` (
  `id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `case_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL,
  `assignedto_id` int(11) DEFAULT NULL,
  `is_selected` tinyint(1) NOT NULL,
  `last_status_change_id` int(11) DEFAULT NULL,
  `is_completed` tinyint(1) NOT NULL,
  `in_progress` int(11) NOT NULL,
  `in_progress_by` int(11) DEFAULT NULL,
  `content_id` int(11) DEFAULT NULL,
  `tested_by` int(11) DEFAULT NULL,
  `tested_on` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `tests`
--

INSERT INTO `tests` (`id`, `run_id`, `case_id`, `status_id`, `assignedto_id`, `is_selected`, `last_status_change_id`, `is_completed`, `in_progress`, `in_progress_by`, `content_id`, `tested_by`, `tested_on`) VALUES
(4, 2, 1, 1, 1, 1, 1, 0, 0, NULL, 1, 1, 1531961056),
(5, 2, 2, 1, 1, 1, 2, 0, 0, NULL, 2, 1, 1531961075),
(6, 2, 3, 1, 1, 1, 5, 0, 0, NULL, 3, 1, 1531961718),
(7, 3, 1, 1, 2, 1, 6, 0, 0, NULL, 1, 2, 1531973027),
(8, 3, 2, 1, 2, 1, 7, 0, 0, NULL, 2, 2, 1531973039),
(9, 3, 3, 1, 2, 1, 8, 0, 0, NULL, 3, 2, 1531973048),
(10, 4, 4, 1, 1, 1, 11, 0, 0, NULL, 4, 1, 1533550970),
(11, 4, 5, 1, 1, 1, 10, 0, 0, NULL, 5, 1, 1533550870),
(12, 4, 6, 1, 1, 1, 9, 0, 0, NULL, 6, 1, 1533550717),
(13, 4, 7, 1, NULL, 1, 14, 0, 0, NULL, 7, 1, 1533551192),
(14, 4, 8, 1, 1, 1, 12, 0, 0, NULL, 8, 1, 1533551055),
(15, 4, 9, 1, NULL, 1, 13, 0, 0, NULL, 9, 1, 1533551105),
(17, 4, 10, 4, NULL, 1, 29, 0, 0, NULL, 10, 1, 1533628692),
(18, 5, 4, 1, 1, 1, 17, 0, 0, NULL, 4, 1, 1533627636),
(19, 5, 5, 1, 1, 1, 16, 0, 0, NULL, 5, 1, 1533627245),
(20, 5, 6, 1, 1, 1, 15, 0, 0, NULL, 6, 1, 1533627063),
(21, 5, 7, 1, 1, 1, 27, 0, 0, NULL, 7, 1, 1533628613),
(22, 5, 8, 1, 1, 1, 23, 0, 0, NULL, 8, 1, 1533628398),
(23, 5, 9, 1, 1, 1, 24, 0, 0, NULL, 9, 1, 1533628498),
(24, 5, 10, 5, 1, 1, 18, 0, 0, NULL, 10, 1, 1533628087);

-- --------------------------------------------------------

--
-- Structure de la table `test_activities`
--

CREATE TABLE `test_activities` (
  `date` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `passed_count` int(11) NOT NULL,
  `retest_count` int(11) NOT NULL,
  `failed_count` int(11) NOT NULL,
  `untested_count` int(11) NOT NULL,
  `blocked_count` int(11) NOT NULL,
  `custom_status1_count` int(11) NOT NULL,
  `custom_status2_count` int(11) NOT NULL,
  `custom_status3_count` int(11) NOT NULL,
  `custom_status4_count` int(11) NOT NULL,
  `custom_status5_count` int(11) NOT NULL,
  `custom_status6_count` int(11) NOT NULL,
  `custom_status7_count` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `test_activities`
--

INSERT INTO `test_activities` (`date`, `project_id`, `run_id`, `passed_count`, `retest_count`, `failed_count`, `untested_count`, `blocked_count`, `custom_status1_count`, `custom_status2_count`, `custom_status3_count`, `custom_status4_count`, `custom_status5_count`, `custom_status6_count`, `custom_status7_count`) VALUES
(20180719, 3, 2, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180719, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180806, 4, 4, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180807, 4, 4, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(20180807, 4, 5, 6, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `test_assocs`
--

CREATE TABLE `test_assocs` (
  `id` int(11) NOT NULL,
  `test_change_id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `test_changes`
--

CREATE TABLE `test_changes` (
  `id` int(11) NOT NULL,
  `test_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL,
  `comment` longtext COLLATE utf8_unicode_ci,
  `version` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `elapsed` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `defects` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_on` int(11) NOT NULL,
  `assignedto_id` int(11) DEFAULT NULL,
  `unassigned` tinyint(1) NOT NULL,
  `project_id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `is_selected` tinyint(1) NOT NULL,
  `caching` int(11) NOT NULL,
  `custom_step_results` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `test_changes`
--

INSERT INTO `test_changes` (`id`, `test_id`, `user_id`, `status_id`, `comment`, `version`, `elapsed`, `defects`, `created_on`, `assignedto_id`, `unassigned`, `project_id`, `run_id`, `is_selected`, `caching`, `custom_step_results`) VALUES
(1, 4, 1, 1, '![](index.php?/attachments/get/10)\n', NULL, NULL, NULL, 1531961056, 1, 0, 3, 2, 1, 1, NULL),
(2, 5, 1, 1, '![](index.php?/attachments/get/11)\n', NULL, NULL, NULL, 1531961075, NULL, 0, 3, 2, 1, 1, NULL),
(3, 6, 1, 1, '![](index.php?/attachments/get/12)\n', NULL, NULL, NULL, 1531961088, NULL, 0, 3, 2, 1, 1, NULL),
(4, 6, 1, 1, NULL, NULL, NULL, NULL, 1531961715, NULL, 0, 3, 2, 1, 1, NULL),
(5, 6, 1, 1, NULL, NULL, NULL, NULL, 1531961718, NULL, 0, 3, 2, 1, 1, NULL),
(6, 7, 2, 1, NULL, NULL, NULL, NULL, 1531973027, 2, 0, 3, 3, 1, 1, NULL),
(7, 8, 2, 1, NULL, NULL, NULL, NULL, 1531973039, 2, 0, 3, 3, 1, 1, NULL),
(8, 9, 2, 1, NULL, NULL, NULL, NULL, 1531973048, 2, 0, 3, 3, 1, 1, NULL),
(9, 12, 1, 1, 'Output:\n2018-08-06T08:41:16.996+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\nMon Aug 06 08:41:18 GMT 2018\nuser lacks privilege or object not found: RESOLUTION in statement [INSERT INTO RESOLUTION values (?,?,?)]![](index.php?/attachments/get/31)\n', NULL, NULL, NULL, 1533550717, 1, 0, 4, 4, 1, 1, NULL),
(10, 11, 1, 1, 'OUTPUT:\n2018-08-06T08:41:12.659+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\nMon Aug 06 08:41:14 GMT 2018\nuser lacks privilege or object not found: RESOLUTION in statement [INSERT INTO RESOLUTION values (?,?,?)]\n![](index.php?/attachments/get/32)', NULL, NULL, NULL, 1533550870, 1, 0, 4, 4, 1, 1, NULL),
(11, 10, 1, 1, 'OUTPUT:\nConnexion reussie\n2018-08-06T08:41:05.961+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\nMon Aug 06 08:41:09 GMT 2018\nuser lacks privilege or object not found: RESOLUTION in statement [INSERT INTO RESOLUTION values (?,?,?)]\n2018-08-06T08:41:09.559+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181![](index.php?/attachments/get/33)\n', NULL, NULL, NULL, 1533550970, 1, 0, 4, 4, 1, 1, NULL),
(12, 14, 1, 1, 'OUTPUT:\nMon Aug 06 08:41:11 GMT 2018\nuser lacks privilege or object not found: RESOLUTION in statement [INSERT INTO RESOLUTION values (?,?,?)]![](index.php?/attachments/get/34)\n', NULL, NULL, NULL, 1533551055, 1, 0, 4, 4, 1, 1, NULL),
(13, 15, 1, 1, 'OUTPUT:\n2018-08-06T08:41:14.858+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\n![](index.php?/attachments/get/35)\n', NULL, NULL, NULL, 1533551105, NULL, 0, 4, 4, 1, 1, NULL),
(14, 13, 1, 1, 'OUTPUT:\n2018-08-06T08:41:11.911+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\n\n![](index.php?/attachments/get/36)\n', NULL, NULL, NULL, 1533551192, NULL, 0, 4, 4, 1, 1, NULL),
(15, 20, 1, 1, 'OUTPUT:\n2018-08-07T07:12:50.038+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\nTue Aug 07 07:12:51 GMT 2018\nuser lacks privilege or object not found: RESOLUTION in statement [INSERT INTO RESOLUTION values (?,?,?)]\n![](index.php?/attachments/get/38)\n', NULL, '2', NULL, 1533627063, NULL, 0, 4, 5, 1, 0, NULL),
(16, 19, 1, 1, 'OUTPUT\n2018-08-07T07:12:44.439+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\nTue Aug 07 07:12:46 GMT 2018\nuser lacks privilege or object not found: RESOLUTION in statement [INSERT INTO RESOLUTION values (?,?,?)]\n![](index.php?/attachments/get/39)\n', NULL, '2.25', NULL, 1533627245, 1, 0, 4, 5, 1, 0, NULL),
(17, 18, 1, 1, 'OUTPUT:\nConnexion reussie\n2018-08-07T07:12:36.263+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\nTue Aug 07 07:12:39 GMT 2018\nuser lacks privilege or object not found: RESOLUTION in statement [INSERT INTO RESOLUTION values (?,?,?)]\n![](index.php?/attachments/get/40)\n', NULL, '4.8', NULL, 1533627636, NULL, 0, 4, 5, 1, 0, NULL),
(18, 24, 1, 5, 'OUTPUT:\n2018-08-07T07:12:46.685+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\norg.testfx.api.FxRobotException: the query \"Historique\" returned no nodes.\nat org.testfx.api.FxRobot.queryVisibleNode(FxRobot.java:969)\nat org.testfx.api.FxRobot.pointOfVisibleNode(FxRobot.java:940)\nat org.testfx.api.FxRobot.moveTo(FxRobot.java:924)\nat org.testfx.api.FxRobot.moveTo(FxRobot.java:60)\nat org.testfx.api.FxRobotInterface.moveTo(FxRobotInterface.java:1250)\nat application.MainTest.testHistorique(MainTest.java:126)\nat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\nat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\nat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\nat java.lang.reflect.Method.invoke(Method.java:498)\nat org.junit.runners.model.FrameworkMethod$1.runReflectiveCall(FrameworkMethod.java:50)\nat org.junit.internal.runners.model.ReflectiveCallable.run(ReflectiveCallable.java:12)\nat org.junit.runners.model.FrameworkMethod.invokeExplosively(FrameworkMethod.java:47)\nat org.junit.internal.runners.statements.InvokeMethod.evaluate(InvokeMethod.java:17)\nat org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)\nat org.junit.internal.runners.statements.RunAfters.evaluate(RunAfters.java:27)\nat org.junit.runners.ParentRunner.runLeaf(ParentRunner.java:325)\nat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:78)\nat org.junit.runners.BlockJUnit4ClassRunner.runChild(BlockJUnit4ClassRunner.java:57)\nat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\nat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\nat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\nat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\nat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\nat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\nat org.junit.runners.Suite.runChild(Suite.java:128)\nat org.junit.runners.Suite.runChild(Suite.java:27)\nat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\nat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\nat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\nat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\nat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\nat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\nat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\nat com.intellij.junit4.JUnit4IdeaTestRunner.startRunnerWithArgs(JUnit4IdeaTestRunner.java:68)\nat com.intellij.rt.execution.junit.IdeaTestRunner$Repeater.startRunnerWithArgs(IdeaTestRunner.java:47)\nat com.intellij.rt.execution.junit.JUnitStarter.prepareStreamsAndStart(JUnitStarter.java:242)\nat com.intellij.rt.execution.junit.JUnitStarter.main(JUnitStarter.java:70)\n![](index.php?/attachments/get/41)\n', NULL, '1', NULL, 1533628087, NULL, 0, 4, 5, 1, 0, NULL),
(19, 24, 1, NULL, NULL, NULL, NULL, NULL, 1533628139, 1, 0, 4, 5, 1, 0, NULL),
(20, 18, 1, NULL, NULL, NULL, NULL, NULL, 1533628158, 1, 0, 4, 5, 1, 0, NULL),
(21, 19, 1, NULL, NULL, NULL, NULL, NULL, 1533628175, 1, 0, 4, 5, 1, 0, NULL),
(22, 20, 1, NULL, NULL, NULL, NULL, NULL, 1533628189, 1, 0, 4, 5, 1, 0, NULL),
(23, 22, 1, 1, 'OUTPUT:\n2018-08-07T07:12:39.923+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\nTue Aug 07 07:12:41 GMT 2018\nuser lacks privilege or object not found: RESOLUTION in statement [INSERT INTO RESOLUTION values (?,?,?)]\n![](index.php?/attachments/get/42)\n', NULL, '2.47', NULL, 1533628398, NULL, 0, 4, 5, 1, 0, NULL),
(24, 23, 1, 1, 'OUTPUT:\n2018-08-07T07:12:47.724+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\n![](index.php?/attachments/get/43)\n', NULL, '2.31', NULL, 1533628498, NULL, 0, 4, 5, 1, 0, NULL),
(25, 22, 1, NULL, NULL, NULL, NULL, NULL, 1533628511, 1, 0, 4, 5, 1, 0, NULL),
(26, 23, 1, NULL, NULL, NULL, NULL, NULL, 1533628520, 1, 0, 4, 5, 1, 0, NULL),
(27, 21, 1, 1, 'OUTPUT:\n2018-08-07T07:12:42.394+0000 WARNING Loading FXML document with JavaFX API of version 9.0.1 by JavaFX runtime of version 8.0.181\n![](index.php?/attachments/get/44)\n', NULL, '2.04', NULL, 1533628613, 1, 0, 4, 5, 1, 0, NULL),
(28, 21, 1, NULL, NULL, NULL, NULL, NULL, 1533628623, 1, 0, 4, 5, 1, 0, NULL),
(29, 17, 1, 4, NULL, NULL, NULL, NULL, 1533628692, NULL, 0, 4, 4, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `test_progress`
--

CREATE TABLE `test_progress` (
  `date` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `run_id` int(11) NOT NULL,
  `tests` int(11) NOT NULL,
  `forecasts` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `test_progress`
--

INSERT INTO `test_progress` (`date`, `project_id`, `run_id`, `tests`, `forecasts`) VALUES
(20180719, 3, 2, -3, 0),
(20180719, 3, 3, -3, 0),
(20180806, 4, 4, -6, 0);

-- --------------------------------------------------------

--
-- Structure de la table `test_timers`
--

CREATE TABLE `test_timers` (
  `test_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `started_on` int(11) NOT NULL,
  `elapsed` int(11) NOT NULL,
  `is_paused` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `uiscripts`
--

CREATE TABLE `uiscripts` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `includes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludes` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `meta` longtext COLLATE utf8_unicode_ci,
  `js` longtext COLLATE utf8_unicode_ci,
  `css` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  `salt` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `rememberme` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `locale` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notifications` tinyint(1) NOT NULL,
  `csrf` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `login_token` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login_token_until` int(11) DEFAULT NULL,
  `last_activity` int(11) DEFAULT NULL,
  `is_reset_password_forced` tinyint(1) NOT NULL DEFAULT '0',
  `data_processing_agreement` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `is_admin`, `salt`, `hash`, `is_active`, `rememberme`, `locale`, `language`, `notifications`, `csrf`, `role_id`, `login_token`, `timezone`, `login_token_until`, `last_activity`, `is_reset_password_forced`, `data_processing_agreement`) VALUES
(1, 'konaesan', 'konaesan@gmail.com', 1, '', '1:$2y$10$OpHCDNR/D53eABtcgzaH0uG0SuDT2oItYUpYYfoftBMYCRVZsT6ym', 1, '', NULL, NULL, 1, '', 1, NULL, NULL, NULL, 1533629101, 0, NULL),
(2, 'Ginette', 'yabohounkponou@gmail.com', 0, '', '1:$2y$10$K5Bae95sjONhkIx19QfdGu47Kt7z6pmApasrJloHIkt12SGbMpP6q', 1, '', 'fr-fr', NULL, 1, '', 1, NULL, NULL, NULL, 1532048871, 0, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user_columns`
--

CREATE TABLE `user_columns` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `columns` longtext COLLATE utf8_unicode_ci NOT NULL,
  `group_by` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `group_order` varchar(250) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_exports`
--

CREATE TABLE `user_exports` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `format` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `options` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_fields`
--

CREATE TABLE `user_fields` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `system_name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `label` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `type_id` int(11) NOT NULL,
  `fallback` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_filters`
--

CREATE TABLE `user_filters` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `filters` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_logins`
--

CREATE TABLE `user_logins` (
  `id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `updated_on` int(11) NOT NULL,
  `attempts` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_logins`
--

INSERT INTO `user_logins` (`id`, `name`, `created_on`, `updated_on`, `attempts`) VALUES
(1, 'konaesan@gmail.com', 1531939714, 1533431432, 0),
(2, 'eliseothniel@gmail.com', 1531972706, 1531972706, 1),
(5, 'yabohounkponou@gmail.com', 1531972882, 1531972882, 0),
(6, 'Ginette', 1533431422, 1533431422, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_settings`
--

CREATE TABLE `user_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `user_tokens`
--

CREATE TABLE `user_tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `name` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `series` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `hash` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_on` int(11) NOT NULL,
  `expires_on` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `user_tokens`
--

INSERT INTO `user_tokens` (`id`, `user_id`, `type_id`, `name`, `series`, `hash`, `created_on`, `expires_on`) VALUES
(3, 2, 2, NULL, '/tmRfLoJoQLsuh05gXcg', '1:$2y$10$WyRIuvGFfreMgTDnAm3eVu8Nw.OXQDzv37v3TnrlANy6YiDQ34ViS', 1531972882, 1534564882),
(4, 1, 2, NULL, 'lc61wdq6pPbZqCu80M3O', '1:$2y$10$UBU3tvCsDwFbral3GX2tmevemkg0BzkM7LNOyutOVkvsb0Bf6kad2', 1533431432, 1536023432);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_attachments_case_id` (`case_id`),
  ADD KEY `ix_attachments_test_change_id` (`test_change_id`);

--
-- Index pour la table `cases`
--
ALTER TABLE `cases`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_cases_section_id` (`section_id`),
  ADD KEY `ix_cases_suite_id` (`suite_id`),
  ADD KEY `ix_cases_copyof_id` (`copyof_id`);

--
-- Index pour la table `case_assocs`
--
ALTER TABLE `case_assocs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_case_assocs_case_id` (`case_id`);

--
-- Index pour la table `case_changes`
--
ALTER TABLE `case_changes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_case_changes_case_id` (`case_id`);

--
-- Index pour la table `case_types`
--
ALTER TABLE `case_types`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `configs`
--
ALTER TABLE `configs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_configs_group_id` (`group_id`);

--
-- Index pour la table `config_groups`
--
ALTER TABLE `config_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_config_groups_project_id` (`project_id`);

--
-- Index pour la table `defects`
--
ALTER TABLE `defects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_defects_defect_id` (`defect_id`),
  ADD KEY `ix_defects_test_change_id` (`test_change_id`),
  ADD KEY `ix_defects_case_id` (`case_id`);

--
-- Index pour la table `exports`
--
ALTER TABLE `exports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_exports_created_on` (`created_on`);

--
-- Index pour la table `fields`
--
ALTER TABLE `fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_fields_name` (`entity_id`,`name`);

--
-- Index pour la table `field_templates`
--
ALTER TABLE `field_templates`
  ADD PRIMARY KEY (`field_id`,`template_id`);

--
-- Index pour la table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `group_users`
--
ALTER TABLE `group_users`
  ADD PRIMARY KEY (`group_id`,`user_id`),
  ADD KEY `ix_group_users_user_id` (`user_id`);

--
-- Index pour la table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_jobs_name` (`name`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `message_recps`
--
ALTER TABLE `message_recps`
  ADD PRIMARY KEY (`message_id`,`user_id`);

--
-- Index pour la table `milestones`
--
ALTER TABLE `milestones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_milestones_project_id` (`project_id`),
  ADD KEY `ix_milestones_parent_id` (`parent_id`);

--
-- Index pour la table `preferences`
--
ALTER TABLE `preferences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_preferences_name` (`user_id`,`name`),
  ADD KEY `ix_preferences_user_id` (`user_id`);

--
-- Index pour la table `priorities`
--
ALTER TABLE `priorities`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `project_access`
--
ALTER TABLE `project_access`
  ADD PRIMARY KEY (`project_id`,`user_id`);

--
-- Index pour la table `project_favs`
--
ALTER TABLE `project_favs`
  ADD PRIMARY KEY (`user_id`,`project_id`);

--
-- Index pour la table `project_groups`
--
ALTER TABLE `project_groups`
  ADD PRIMARY KEY (`project_id`,`group_id`);

--
-- Index pour la table `project_history`
--
ALTER TABLE `project_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_project_history_project_order` (`project_id`,`created_on`);

--
-- Index pour la table `refs`
--
ALTER TABLE `refs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_refs_reference_id` (`reference_id`),
  ADD KEY `ix_refs_case_id` (`case_id`);

--
-- Index pour la table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_reports_project_id` (`project_id`);

--
-- Index pour la table `report_jobs`
--
ALTER TABLE `report_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_report_jobs_project_id` (`project_id`);

--
-- Index pour la table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `runs`
--
ALTER TABLE `runs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_runs_project_id` (`project_id`),
  ADD KEY `ix_runs_plan_id` (`plan_id`),
  ADD KEY `ix_runs_milestone_id` (`milestone_id`),
  ADD KEY `ix_runs_suite_id` (`suite_id`);

--
-- Index pour la table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_sections_suite_id` (`suite_id`),
  ADD KEY `ix_sections_copyof_id` (`copyof_id`),
  ADD KEY `ix_sections_parent_id` (`parent_id`);

--
-- Index pour la table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_sessions_session_id` (`session_id`);

--
-- Index pour la table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_settings_name` (`name`);

--
-- Index pour la table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_statuses_name` (`name`);

--
-- Index pour la table `subscriptions`
--
ALTER TABLE `subscriptions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_subscriptions_run_test` (`run_id`,`test_id`,`user_id`);

--
-- Index pour la table `suites`
--
ALTER TABLE `suites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_suites_project_id` (`project_id`),
  ADD KEY `ix_suites_copyof_id` (`copyof_id`);

--
-- Index pour la table `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `templates`
--
ALTER TABLE `templates`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `template_projects`
--
ALTER TABLE `template_projects`
  ADD PRIMARY KEY (`template_id`,`project_id`);

--
-- Index pour la table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_tests_run_id` (`run_id`),
  ADD KEY `ix_tests_case_id` (`case_id`,`is_selected`),
  ADD KEY `ix_tests_content_id` (`content_id`);

--
-- Index pour la table `test_activities`
--
ALTER TABLE `test_activities`
  ADD PRIMARY KEY (`date`,`project_id`,`run_id`),
  ADD KEY `ix_test_activities_run_id` (`run_id`);

--
-- Index pour la table `test_assocs`
--
ALTER TABLE `test_assocs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_test_assocs_test_change_id` (`test_change_id`),
  ADD KEY `ix_test_assocs_test_id` (`test_id`);

--
-- Index pour la table `test_changes`
--
ALTER TABLE `test_changes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_test_changes_test_id` (`test_id`),
  ADD KEY `ix_test_changes_project_order` (`project_id`,`is_selected`,`created_on`),
  ADD KEY `ix_test_changes_run_order` (`run_id`,`is_selected`,`created_on`);

--
-- Index pour la table `test_progress`
--
ALTER TABLE `test_progress`
  ADD PRIMARY KEY (`date`,`project_id`,`run_id`),
  ADD KEY `ix_test_progress_run_id` (`run_id`);

--
-- Index pour la table `test_timers`
--
ALTER TABLE `test_timers`
  ADD PRIMARY KEY (`test_id`,`user_id`),
  ADD KEY `ix_test_timers_user_id` (`user_id`);

--
-- Index pour la table `uiscripts`
--
ALTER TABLE `uiscripts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_users_email` (`email`);

--
-- Index pour la table `user_columns`
--
ALTER TABLE `user_columns`
  ADD PRIMARY KEY (`user_id`,`project_id`,`area_id`);

--
-- Index pour la table `user_exports`
--
ALTER TABLE `user_exports`
  ADD PRIMARY KEY (`user_id`,`project_id`,`area_id`,`format`);

--
-- Index pour la table `user_fields`
--
ALTER TABLE `user_fields`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_user_fields_name` (`name`);

--
-- Index pour la table `user_filters`
--
ALTER TABLE `user_filters`
  ADD PRIMARY KEY (`user_id`,`project_id`,`area_id`);

--
-- Index pour la table `user_logins`
--
ALTER TABLE `user_logins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_user_logins_name` (`name`);

--
-- Index pour la table `user_settings`
--
ALTER TABLE `user_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ux_user_settings_name` (`user_id`,`name`);

--
-- Index pour la table `user_tokens`
--
ALTER TABLE `user_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ix_user_tokens_user_id` (`user_id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT pour la table `cases`
--
ALTER TABLE `cases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `case_assocs`
--
ALTER TABLE `case_assocs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `case_changes`
--
ALTER TABLE `case_changes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `case_types`
--
ALTER TABLE `case_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `configs`
--
ALTER TABLE `configs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `config_groups`
--
ALTER TABLE `config_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `defects`
--
ALTER TABLE `defects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `exports`
--
ALTER TABLE `exports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `fields`
--
ALTER TABLE `fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `milestones`
--
ALTER TABLE `milestones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `preferences`
--
ALTER TABLE `preferences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `priorities`
--
ALTER TABLE `priorities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `project_history`
--
ALTER TABLE `project_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `refs`
--
ALTER TABLE `refs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT pour la table `report_jobs`
--
ALTER TABLE `report_jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `runs`
--
ALTER TABLE `runs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT pour la table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `subscriptions`
--
ALTER TABLE `subscriptions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `suites`
--
ALTER TABLE `suites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `templates`
--
ALTER TABLE `templates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `test_assocs`
--
ALTER TABLE `test_assocs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `test_changes`
--
ALTER TABLE `test_changes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT pour la table `uiscripts`
--
ALTER TABLE `uiscripts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `user_fields`
--
ALTER TABLE `user_fields`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_logins`
--
ALTER TABLE `user_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT pour la table `user_settings`
--
ALTER TABLE `user_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user_tokens`
--
ALTER TABLE `user_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
